package com.eugenegolobokin.covid19tracker.controllers;

import com.eugenegolobokin.covid19tracker.models.LocationStats;
import com.eugenegolobokin.covid19tracker.services.Covid19DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    Covid19DataService covid19DataService;

    // mapping to root URL with just "/"
    @GetMapping("/")
    // we can put things into Model object and thymeleaf will make it accessible in rendered html
    public String home(Model model) {
        List<LocationStats> allStats = covid19DataService.getAllStats();
        int totalReportedCases = allStats.stream().mapToInt(stat -> stat.getLatestTotalCases()).sum();
        int totalNewCases = allStats.stream().mapToInt(stat -> stat.getDifferenceFromPreviousDay()).sum();

        // saving attributes into models to use in html thymeleaf representation
        model.addAttribute("locationStats", allStats);
        model.addAttribute("totalReportedCases", totalReportedCases);
        model.addAttribute("totalNewCases", totalNewCases);

        // as it is annotated as Spring @Controller
        // return name which point to a template - should be mapped to home.html
        // works because we have thymeleaf dependency
        return "home";
    }

}
