# COVID19 tracker app

Based on workshop by javabrains.io: 
https://www.youtube.com/watch?v=8hjNG9GZGnQ

Data source used in project: 
https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv

Used:
- Spring Boot
- Bootstrap
- Apache commons CSV